package tn.project.stopcorona.mailing.model;

import java.util.Map;

public class EmailData {

    private final String email;
    private final Map<String, Object> contextVariables;

    public EmailData(String email, Map<String, Object> contextVariables) {
        this.email = email;
        this.contextVariables = contextVariables;
    }

    public String getEmail() {
        return email;
    }

    public Map<String, Object> getContextVariables() {
        return contextVariables;
    }
}
