package tn.project.stopcorona.mailing;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;
import tn.project.stopcorona.mailing.model.EmailData;

import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

@Component
@EnableAsync
public class StopCoronaMailSender {

    private static final String CHARSET_UTF_8_NAME = StandardCharsets.UTF_8.name();
    private static final String NEW_PATIENT_EMAIL_TEMPLATE_NAME = "layouts/doctorTemplate";
    private static final String NEW_PATIENT_EMAIL_SUBJECT = "Notification nouveau patients connectés";

    private final JavaMailSender javaMailSender;
    private final ITemplateEngine templateEngine;
    private String applicationEmail;

    StopCoronaMailSender(JavaMailSender javaMailSender, ITemplateEngine templateEngine,
                         @Value("${stop-corona.mail}") String stopCoronaEmail) {
        this.javaMailSender = javaMailSender;
        this.templateEngine = templateEngine;
        this.applicationEmail = stopCoronaEmail;
    }

    @Async
    public void sendEmailToAllDoctors(List<EmailData> emailData) {
        for (EmailData data : emailData) {
            sendEmail(applicationEmail, data.getEmail(), createContext(NEW_PATIENT_EMAIL_TEMPLATE_NAME, data.getContextVariables()),
                    NEW_PATIENT_EMAIL_SUBJECT);
        }
    }

    public String createContext(String templateName, Map<String, Object> contextVariables) {
        final Context context = new Context();
        context.setVariables(contextVariables);
        return templateEngine.process(templateName, context);
    }


    private void sendEmail(String from, String to, String content, String subject) {
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, CHARSET_UTF_8_NAME);
            message.setText(content, true);
            message.setTo(to);
            message.setFrom(from);
            message.setSubject(subject);
            javaMailSender.send(mimeMessage);
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
}
