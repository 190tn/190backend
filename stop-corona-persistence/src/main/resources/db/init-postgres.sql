insert into question (id, text)
VALUES (1, 'كنت في سفر أو قابلتشي شخص مريض بالفيروس او حد رجع من سفر في 14 يوم الي فاتو؟ وإلاّ تسكنشي أو كنتشي في أحد هذه المناطق الموبوءة');
update question
set question_order = 100,
    score          = 2
where id = 1;
insert into question
VALUES (2, 'عندكشي سخانة فوق 37.8 ؟');
update question
set question_order = 200,
    score          = 2
where id = 2;
insert into question
VALUES (3, 'عندكشي كحة شايحة او صعوبة في التنفس ؟');
update question
set question_order = 300,
    score          = 2
where id = 3;

insert into question
VALUES (4, 'عندكشي حراق في القراجم او الحلق ؟');
update question
set question_order = 400,
    score          = 1
where id = 4;
insert into question
VALUES (5, 'اسهال تباوع ،ترد، كرشك تجري ؟');
update question
set question_order = 500,
    score          = 1
where id = 5;
insert into question
VALUES (6, 'عندكشي قصورمزمن في التنفس او قصور كلوي مزمن او قصور مزمن في القلب؟');
update question
set question_order = 600,
    score          = 1
where id = 6;


insert into response
VALUES (1, 'لا', 0);
insert into response
VALUES (2, 'نعم', 1);

insert into question_response
VALUES (1, 1, 1);
insert into question_response
VALUES (2, 1, 2);
insert into question_response
VALUES (3, 2, 1);
insert into question_response
VALUES (4, 2, 2);
insert into question_response
VALUES (5, 3, 1);
insert into question_response
VALUES (6, 3, 2);
insert into question_response
VALUES (7, 4, 1);
insert into question_response
VALUES (8, 4, 2);
insert into question_response
VALUES (9, 5, 1);
insert into question_response
VALUES (10, 5, 2);
insert into question_response
VALUES (11, 6, 1);
insert into question_response
VALUES (12, 6, 2);

insert into region
VALUES (1, 'أريانة');
insert into region
VALUES (2, 'باجة');
insert into region
VALUES (3, 'بن عروس');
insert into region
VALUES (4, 'بنزرت');
insert into region
VALUES (5, 'قابس');
insert into region
VALUES (6, 'قفصة');
insert into region
VALUES (7, 'جندوبة');
insert into region
VALUES (8, 'القيروان');
insert into region
VALUES (9, 'القصرين');
insert into region
VALUES (10, 'قبلي');
insert into region
VALUES (11, 'الكاف');
insert into region
VALUES (12, 'المهدية');
insert into region
VALUES (13, 'منوبة');
insert into region
VALUES (14, 'مدنين');
insert into region
VALUES (15, 'المنستير');
insert into region
VALUES (16, 'نابل');
insert into region
VALUES (17, 'صفاقس');
insert into region
VALUES (18, 'سيدي بوزيد');
insert into region
VALUES (19, 'سليانة');
insert into region
VALUES (20, 'سوسة');
insert into region
VALUES (21, 'تطاوين');
insert into region
VALUES (22, 'توزر');
insert into region
VALUES (23, 'تونس');
insert into region
VALUES (24, 'زغوان');
INSERT INTO doctor (id, email, encrypted_password, full_name, phone_number, speciality, user_name)
VALUES (1, 'test@gmail.com', '{bcrypt}$2a$10$8mq/AGDIBMx6to32gYY4iOHaSJDFZ1zyQw.kwUSIBDq/i82ofZDcy', 'test test',
        '99 710 955', 'PSI', 'test');
