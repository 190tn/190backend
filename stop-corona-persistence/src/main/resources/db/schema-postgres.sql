CREATE TABLE question
    (
        id	      INTEGER NOT NULL,
        text      VARCHAR(254) NOT NULL,
        question_order     INTEGER,
        score     INTEGER,
        PRIMARY KEY (id)
    );

CREATE TABLE response
    (
        id	      INTEGER NOT NULL,
        text      VARCHAR(5) NOT NULL,
        score     INTEGER NOT NULL,
        PRIMARY KEY (id)
    );

CREATE TABLE question_response
    (
        id	            INTEGER NOT NULL,
        question_id    INTEGER,
        response_id     INTEGER,
        PRIMARY KEY (id),
        FOREIGN KEY (question_id) REFERENCES Question (id),
        FOREIGN KEY (response_id) REFERENCES response (id)
    );

CREATE TABLE doctor
    (
        id	                INTEGER NOT NULL,
        full_name           VARCHAR(254) NOT NULL,
        user_name           VARCHAR(254) NOT NULL,
        encrypted_password  VARCHAR(254) NOT NULL,
        phone_number        VARCHAR(254) NOT NULL,
        email                VARCHAR(254),
        speciality          VARCHAR(254),
        PRIMARY KEY (id)
    );

CREATE TABLE form
    (
        id	            INTEGER NOT NULL,
        question_id    INTEGER,
        response_id     INTEGER,
        patient_id     INTEGER,
        PRIMARY KEY (id),
        FOREIGN KEY (question_id) REFERENCES Question (id),
        FOREIGN KEY (response_id) REFERENCES response (id),
        FOREIGN KEY (patient_id) REFERENCES doctor (id)
    );
CREATE TABLE region
    (
        id	                INTEGER NOT NULL,
        name           VARCHAR(254) NOT NULL,
        PRIMARY KEY (id)
    );
CREATE TABLE patient
(
        id serial NOT NULL,
        full_name VARCHAR(254) NOT NULL,
        phone_number VARCHAR(254) NOT NULL,
        request_score INTEGER,
        request_date DATE ,
        request_status INT,
        region_id INTEGER,
        doctor_id      INTEGER,
        FOREIGN KEY (region_id) REFERENCES region (id),
        FOREIGN KEY (doctor_id) REFERENCES doctor (id),
        PRIMARY KEY (id)
);



--- Added by Mohamed Kallel 25/03/2020 22:05
ALTER TABLE patient
    ADD session_id varchar(128);
ALTER TABLE patient
    ADD uuid varchar(128);
ALTER TABLE patient
    ADD connected boolean;