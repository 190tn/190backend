package tn.project.stopcorona.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.project.stopcorona.persistence.model.Question;

import java.util.List;

public interface QuestionRepository extends JpaRepository<Question, Integer> {
    public List<Question> findAllByOrderByQuestionOrder();
}
