package tn.project.stopcorona.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "response")
public class Response extends Identifiable<Integer> {
    private static final long serialVersionUID = 931701396773039764L;

    @Column(name = "text", nullable = false, unique = true)
    private String text;

    @Column
    private int score;

}
