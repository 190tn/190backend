package tn.project.stopcorona.persistence.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import tn.project.stopcorona.persistence.model.Patient;
import tn.project.stopcorona.persistence.model.RequestStatus;

import java.util.List;
import java.util.stream.Stream;

public interface PatientRepository extends JpaRepository<Patient, Integer> {
    @Query("SELECT p FROM Patient p left join fetch p.region left join fetch p.delegation WHERE p.requestStatus =tn.project.stopcorona.persistence.model.RequestStatus.IN_PROGRESS order by p.requestScore desc, p.requestDate asc")
    List<Patient> findByRequestStatus();

    @Query("select p from Patient p where lower(p.fullName) = lower(:name) and p.phoneNumber = :phoneNumber order by p.requestScore desc, p.requestDate asc")
    Patient findUuidByNameAndPhoneNumber(@Param("name") String name, @Param("phoneNumber") String phoneNumber);

    @Query(value = "SELECT p FROM Patient p left join fetch p.forms f left join fetch f.question left join fetch f.response WHERE p.requestStatus =tn.project.stopcorona.persistence.model.RequestStatus.IN_PROGRESS" +
            " AND p.connected=true AND p.callInProgress=false order by p.requestScore desc, p.requestDate asc")
    List<Patient> findFirstConnectedPatient(Pageable pageable, @Param("connectedDoctorId") Integer connectedDoctorId);

    @Modifying
    @Query(value = "UPDATE patient SET doctor_id =:doctorId WHERE id=:id", nativeQuery = true)
    int updatePatient(@Param("id") Integer id,
                      @Param("doctorId") Integer doctorId);

    @Modifying
    @Query(value = "UPDATE Patient SET doctor=(select d from Doctor d where userName=:doctorUserName), requestStatus=:status, doctorComment=:comment WHERE id=:patientId")
    void setRequestStatus(@Param("patientId") Integer patientId,@Param("doctorUserName") String doctorUsername, @Param("status") RequestStatus status, @Param("comment") String comment);

    List<Patient> findByRequestStatusAndDoctorUserName(RequestStatus status, String userName);
}
