package tn.project.stopcorona.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.project.stopcorona.persistence.model.Doctor;

import java.util.Optional;

public interface DoctorRepository extends JpaRepository<Doctor, Integer> {

    Optional<Doctor> findByUserName(String userName);
}
