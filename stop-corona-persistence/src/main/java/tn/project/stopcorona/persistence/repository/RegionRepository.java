package tn.project.stopcorona.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tn.project.stopcorona.persistence.model.Region;

import java.util.List;

public interface RegionRepository extends JpaRepository<Region, Integer> {

    @Query("select distinct(r) from Region r left join fetch r.delegations")
    List<Region> findAllWithDelegations();
}
