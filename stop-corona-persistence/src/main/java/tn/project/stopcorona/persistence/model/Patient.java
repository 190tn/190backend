package tn.project.stopcorona.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "patient")
public class Patient extends Identifiable<Integer> {

    private static final long serialVersionUID = -4657487762533509509L;

    @Column(name = "full_name", nullable = false)
    private String fullName;

    @Column(name = "phone_number", nullable = false, unique = true)
    private String phoneNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "region_id", nullable = false)
    private Region region;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "delegation_id", nullable = false)
    private Delegation delegation;


    @Column(name = "request_score", nullable = false)
    private Integer requestScore;

    @Column(name = "request_date", nullable = false)
    private LocalDateTime requestDate;

    @Column(name = "request_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private RequestStatus requestStatus;

    @OneToMany(mappedBy = "patient", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Form> forms = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "doctor_id", nullable = false)
    private Doctor doctor;

    @Column(name = "sessionId")
    private String sessionId;

    @Column(name = "uuid")
    private String uuid;

    @Column(name = "connected")
    private Boolean connected;

    @Column(name = "call_in_progress")
    private Boolean callInProgress;

    @Column(name = "doctor_comment")
    private String doctorComment;


    @PrePersist
    public void beforePersist() {
        this.requestDate = LocalDateTime.now();
        this.requestStatus = RequestStatus.IN_PROGRESS;
        this.callInProgress = false;
    }

    public void addForm(Form form) {
        forms.add(form);
        form.setPatient(this);
    }

    public void removeForm(Form form) {
        forms.remove(form);
        form.setPatient(null);
    }

}
