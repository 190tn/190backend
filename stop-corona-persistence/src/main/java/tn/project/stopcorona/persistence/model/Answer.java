package tn.project.stopcorona.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "answer")
public class Answer extends Identifiable<Integer> {

    private static final long serialVersionUID = 8969738037068706570L;

    @Column(name = "text", nullable = false, unique = true)
    private String text;
}
