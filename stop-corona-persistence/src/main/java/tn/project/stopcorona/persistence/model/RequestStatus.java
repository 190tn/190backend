package tn.project.stopcorona.persistence.model;

public enum RequestStatus {
    IN_PROGRESS,
    NEED_EMERGENCY,
    CALL_STOPPED,
    DIRECT_CALL,
    PROCESSED
}
