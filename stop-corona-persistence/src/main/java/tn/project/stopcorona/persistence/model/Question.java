package tn.project.stopcorona.persistence.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "question")
public class Question extends Identifiable<Integer> {

    private static final long serialVersionUID = 931701396773039764L;

    @Column(name = "text", nullable = false, unique = true)
    private String text;

    @Column(name = "question_order", nullable = false, unique = true)
    private Integer questionOrder;

    @Column(name = "score", nullable = false)
    private Integer score;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "question_response",
            joinColumns = {@JoinColumn(name = "question_id")},
            inverseJoinColumns = {@JoinColumn(name = "response_id")}
    )
    private List<Response> response = new ArrayList<>();

    @Column(name = "advice")
    private String advice;
}
