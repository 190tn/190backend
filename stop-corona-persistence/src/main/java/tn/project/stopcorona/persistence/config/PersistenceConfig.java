package tn.project.stopcorona.persistence.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("tn.project.stopcorona.persistence.repository")
@EntityScan("tn.project.stopcorona.persistence.model")
public class PersistenceConfig {
}
