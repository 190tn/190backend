package tn.project.stopcorona.api.security;

import org.springframework.stereotype.Component;
import tn.project.stopcorona.idp.PublicUrlsProvider;

@Component
public class StopCoronaPublicUrlsProvider implements PublicUrlsProvider {

    private static final String[] PUBLIC_URLS = {
            "/doctors/sign-in",
            "/questions/*",
            "/regions/all",
            "/doctors/is-connected",
            "/socket/patient",
            "/patients/notify",
            "/resources/*",
            "/oauth/token/*"
    };

    @Override
    public String[] getPublicUrls() {
        return PUBLIC_URLS;
    }
}
