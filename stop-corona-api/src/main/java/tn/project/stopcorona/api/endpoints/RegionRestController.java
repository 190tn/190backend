package tn.project.stopcorona.api.endpoints;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tn.project.stopcorona.business.dto.RegionDTO;
import tn.project.stopcorona.business.service.RegionService;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/regions")
public class RegionRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegionRestController.class);
    private final RegionService regionService;

    RegionRestController(RegionService regionService) {
        this.regionService = regionService;
    }

    @GetMapping("all")
    public ResponseEntity<List<RegionDTO>> getAllRegions() {
        try {
            return new ResponseEntity<>(regionService.getAll(), HttpStatus.OK);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return new ResponseEntity<>(Collections.emptyList(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
