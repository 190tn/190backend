package tn.project.stopcorona.api.security.principal;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import tn.project.stopcorona.api.security.principal.exceptions.TokenExpiredException;

@Component
public class PrincipalProvider {

    public UserDetails getConnectedUser() {
        final Object principal = SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        if (principal instanceof UserDetails) {
            return (UserDetails) principal;
        }
        throw new TokenExpiredException("The current token is expired");
    }
}
