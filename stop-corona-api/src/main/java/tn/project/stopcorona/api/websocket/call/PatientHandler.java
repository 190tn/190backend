package tn.project.stopcorona.api.websocket.call;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import tn.project.stopcorona.business.dto.PatientDTOWithConnectedNumber;
import tn.project.stopcorona.business.service.PatientService;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

@Component
public class PatientHandler extends TextWebSocketHandler {

    public static List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();
    private static final String UPDATE_TYPE = "UPDATE";

    private final PatientService patientService;
    private final MappingJackson2HttpMessageConverter converter;


    PatientHandler(PatientService patientService,
                   MappingJackson2HttpMessageConverter converter) {
        this.patientService = patientService;
        this.converter = converter;
    }


    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message)
            throws InterruptedException, IOException {
        /*for (WebSocketSession webSocketSession : sessions) {
            if (webSocketSession.isOpen() && !session.getId().equals(webSocketSession.getId())) {
                webSocketSession.sendMessage(message);
            }
        }*/
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {

        sessions.add(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        sessions.removeAll(sessions.stream().filter(s -> s.getId().equals(session.getId())).collect(Collectors.toList()));
    }

    public void sendUpdate() throws JsonProcessingException {
        if (sessions.isEmpty()) {
            return;
        }
        PatientDTOWithConnectedNumber patients = patientService.getInProgressPatients();
        PatientSocketData data = new PatientSocketData();
        data.setType(UPDATE_TYPE);
        data.setData(patients);
        String jsonData = converter.getObjectMapper().writeValueAsString(data);
        sessions.stream().forEach(session -> {
            try {
                session.sendMessage(new TextMessage(jsonData));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

    }

}
