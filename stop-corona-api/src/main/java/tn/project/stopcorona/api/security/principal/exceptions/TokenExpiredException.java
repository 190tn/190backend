package tn.project.stopcorona.api.security.principal.exceptions;

public class TokenExpiredException extends RuntimeException {

    private static final long serialVersionUID = 5491393613463640349L;

    public TokenExpiredException(String message) {
        super(message);
    }
}
