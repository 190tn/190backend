package tn.project.stopcorona.api.endpoints;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/doctors")
public class DoctorRestController {

    @GetMapping("is-connected")
    public ResponseEntity<Boolean> isConnected() {
        try {
            Object connectedUser = SecurityContextHolder
                    .getContext()
                    .getAuthentication()
                    .getPrincipal();
            boolean isConnected = connectedUser != null && !"anonymousUser".equals(connectedUser);
            return new ResponseEntity<>(isConnected, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(false, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("logout")
    public void logout() {
        SecurityContextHolder
                .getContext()
                .getAuthentication()
                .setAuthenticated(false);
    }

}
