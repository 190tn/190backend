package tn.project.stopcorona.api.endpoints;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.project.stopcorona.api.security.principal.PrincipalProvider;
import tn.project.stopcorona.api.websocket.call.PatientHandler;
import tn.project.stopcorona.business.dto.PatientDTO;
import tn.project.stopcorona.business.dto.PatientDTOWithConnectedNumber;
import tn.project.stopcorona.business.service.PatientService;
import tn.project.stopcorona.persistence.model.RequestStatus;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/patients")
public class PatientRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientRestController.class);
    private final PatientService patientService;
    private final PrincipalProvider principalProvider;
    private final PatientHandler patientHandler;

    PatientRestController(PatientService patientService,
                          PrincipalProvider principalProvider, PatientHandler patientHandler) {
        this.patientService = patientService;
        this.principalProvider = principalProvider;
        this.patientHandler = patientHandler;
    }

    @GetMapping("in-progress")
    public ResponseEntity<PatientDTOWithConnectedNumber> getInProgressPatients(RequestStatus requestStatus) {
        try {
            return new ResponseEntity<>(patientService.getInProgressPatients(), HttpStatus.OK);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("first-to-call")
    public ResponseEntity<PatientDTO> getFirstConnectedPatientForCall() {
        try {
            return new ResponseEntity<>(patientService.getFirstConnectedPatientForCall(
                    principalProvider.getConnectedUser().getUsername()), HttpStatus.OK);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("{patientId}/{status}")
    public Boolean directCall(@PathVariable("patientId") Integer patientId, @PathVariable("status") RequestStatus status, @RequestBody Optional<Map> body) throws JsonProcessingException {
        this.patientService.setRequestStatus(
                patientId,
                this.principalProvider.getConnectedUser().getUsername(),
                status,
                body
                        .map(b -> b.get("comment"))
                        .map(c -> (String)c)
                        .orElse(null)
        );
        this.patientHandler.sendUpdate();
        return true;
    }

    @GetMapping("direct-call")
    public PatientDTO directCall() {
        return this.patientService.getDirectCall(this.principalProvider.getConnectedUser().getUsername());
    }

    @GetMapping("notify")
    public ResponseEntity<Boolean> notifySessions() throws JsonProcessingException {
        patientHandler.sendUpdate();
        return ResponseEntity.ok(true);
    }
}
