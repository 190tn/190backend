package tn.project.stopcorona.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "tn.project.stopcorona")
public class StopCoronaApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(StopCoronaApiApplication.class, args);
    }
    
}
