package tn.project.stopcorona.api.websocket.call;

import java.io.Serializable;

public class PatientSocketData {
    private String type;
    private Object data;


    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
