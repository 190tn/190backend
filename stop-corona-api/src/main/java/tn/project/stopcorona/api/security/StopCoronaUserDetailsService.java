package tn.project.stopcorona.api.security;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import tn.project.stopcorona.persistence.model.Doctor;
import tn.project.stopcorona.persistence.repository.DoctorRepository;

import java.util.Collections;

@Service
public class StopCoronaUserDetailsService implements UserDetailsService {

    private final DoctorRepository doctorRepository;

    StopCoronaUserDetailsService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        if (StringUtils.isEmpty(userName)) {
            throw new IllegalArgumentException("userName is null or empty");
        }

        Doctor doctor = doctorRepository.findByUserName(userName)
                .orElseThrow(() -> new BadCredentialsException("Doctor with user name " + userName + " is not found"));

        return new User(doctor.getUserName(),
                doctor.getEncryptedPassword(),
                Collections.emptyList());
    }
}
