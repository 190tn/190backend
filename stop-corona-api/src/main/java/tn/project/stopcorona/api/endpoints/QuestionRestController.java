package tn.project.stopcorona.api.endpoints;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tn.project.stopcorona.api.websocket.call.PatientHandler;
import tn.project.stopcorona.business.dto.PatientDTO;
import tn.project.stopcorona.business.dto.QuestionDTO;
import tn.project.stopcorona.business.service.PatientService;
import tn.project.stopcorona.business.service.QuestionService;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/questions")
public class QuestionRestController {
    private final Logger log = LoggerFactory.getLogger(QuestionRestController.class);

    private final QuestionService questionService;
    private final PatientService patientService;
    private final PatientHandler patientHandler;

    QuestionRestController(QuestionService questionService, PatientService patientService, PatientHandler patientHandler) {
        this.questionService = questionService;
        this.patientService = patientService;
        this.patientHandler = patientHandler;
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<QuestionDTO>> get() {
        try {
            log.info(("####### Start test #######"));
            return new ResponseEntity<>(questionService.findAllQuestions(), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(Collections.emptyList(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/save")
    public ResponseEntity<PatientDTO> saveForm(@RequestBody PatientDTO patientDTO) {
        try {
            PatientDTO result = patientService.addPatient(patientDTO);
            this.patientHandler.sendUpdate();
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
