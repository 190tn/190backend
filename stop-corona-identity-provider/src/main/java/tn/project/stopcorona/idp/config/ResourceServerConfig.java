package tn.project.stopcorona.idp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import tn.project.stopcorona.idp.PublicUrlsProvider;

@Configuration
@EnableResourceServer
public class ResourceServerConfig implements ResourceServerConfigurer {

    private static final String RESOURCE_ID = "stop-corona-resource-id";

    @Autowired
    private PublicUrlsProvider publicUrlsProvider;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(RESOURCE_ID).stateless(false);
    }

    @Override
    public void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.cors().disable()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(publicUrlsProvider.getPublicUrls())
                .permitAll()
                .anyRequest().authenticated();
    }


}
