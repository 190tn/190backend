package tn.project.stopcorona.idp;

public interface PublicUrlsProvider {

    String[] getPublicUrls();
}
