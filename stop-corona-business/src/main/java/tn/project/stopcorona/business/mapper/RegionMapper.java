package tn.project.stopcorona.business.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import tn.project.stopcorona.business.dto.QuestionDTO;
import tn.project.stopcorona.business.dto.RegionDTO;
import tn.project.stopcorona.persistence.model.Question;
import tn.project.stopcorona.persistence.model.Region;

@Mapper(uses = {DelegationMapper.class})
public interface RegionMapper {

    RegionMapper INSTANCE = Mappers.getMapper(RegionMapper.class);

    RegionDTO entityToDto(Region region);

    Question dtoToEntity(QuestionDTO questionDTO);
}
