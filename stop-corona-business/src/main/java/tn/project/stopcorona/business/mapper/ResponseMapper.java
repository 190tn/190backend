package tn.project.stopcorona.business.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import tn.project.stopcorona.business.dto.ResponseDTO;
import tn.project.stopcorona.persistence.model.Response;

@Mapper
public interface ResponseMapper {
    ResponseMapper INSTANCE = Mappers.getMapper(ResponseMapper.class);

    ResponseDTO entiryToDto(Response response);

    Response dtoToEntity(ResponseDTO responseDTO);
}
