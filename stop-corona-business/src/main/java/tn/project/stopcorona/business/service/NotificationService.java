package tn.project.stopcorona.business.service;

import org.springframework.stereotype.Service;
import tn.project.stopcorona.mailing.StopCoronaMailSender;
import tn.project.stopcorona.mailing.model.EmailData;
import tn.project.stopcorona.persistence.model.Doctor;
import tn.project.stopcorona.persistence.repository.DoctorRepository;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class NotificationService {

    private static final String ONE_SIGNAL_NOTIFICATIONS_API_PATH = "https://onesignal.com/api/v1/notifications";
    private static final String ONE_SIGNAL_APP_ID = "283848e5-c3b5-4231-833e-f63679b19b0f";
    private static final String ONE_SIGNAL_API_KEY = "Njk5NjM5N2EtMDY1MS00YzU0LWI5YzYtZWNhZTAyNzEzMzY4";

    private final DoctorRepository doctorRepository;
    private final StopCoronaMailSender mailSender;

    NotificationService(DoctorRepository doctorRepository,
                        StopCoronaMailSender mailSender) {
        this.doctorRepository = doctorRepository;
        this.mailSender = mailSender;
    }

    public void sendNotificationToSubscribedDoctors() {
        try {
            URL url = new URL(ONE_SIGNAL_NOTIFICATIONS_API_PATH);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setUseCaches(false);
            con.setDoOutput(true);
            con.setDoInput(true);

            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            con.setRequestProperty("Authorization", "Basic " + ONE_SIGNAL_API_KEY);
            con.setRequestMethod("POST");

            String jsonBody = "{"
                    + "\"app_id\":" + "\"" + ONE_SIGNAL_APP_ID + "\", "
                    + "\"included_segments\": [\"All\"],"
                    + "\"contents\": {\"en\": \"Il'ya des nouveaux patients connectés\"}"
                    + "}";

            byte[] sendBytes = jsonBody.getBytes(StandardCharsets.UTF_8);
            con.setFixedLengthStreamingMode(sendBytes.length);

            OutputStream outputStream = con.getOutputStream();
            outputStream.write(sendBytes);
            con.getResponseCode();

        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    public void sendEmailToAllDoctors() {
        List<Doctor> doctors = doctorRepository.findAll();
        if (doctors != null && !doctors.isEmpty()) {
            mailSender.sendEmailToAllDoctors(doctors.stream()
                    .map(doctor -> {
                        Map<String, Object> contextVariables = new HashMap<>();
                        contextVariables.put("fullName", doctor.getFullName());
                        return new EmailData(doctor.getEmail(), contextVariables);
                    })
                    .collect(Collectors.toList()));
        }
    }
}
