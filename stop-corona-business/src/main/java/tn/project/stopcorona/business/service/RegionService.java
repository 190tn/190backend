package tn.project.stopcorona.business.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.project.stopcorona.business.dto.RegionDTO;
import tn.project.stopcorona.business.mapper.RegionMapper;
import tn.project.stopcorona.persistence.model.Region;
import tn.project.stopcorona.persistence.repository.RegionRepository;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RegionService {

    private final RegionRepository regionRepository;
    private final RegionMapper regionMapper = RegionMapper.INSTANCE;

    RegionService(RegionRepository regionRepository) {
        this.regionRepository = regionRepository;
    }

    @Transactional(readOnly = true)
    public List<RegionDTO> getAll() {
        List<Region> regions = regionRepository.findAllWithDelegations();
        return regions != null ? regions.stream()
                .map(regionMapper::entityToDto)
                .collect(Collectors.toList()) : Collections.emptyList();
    }
}
