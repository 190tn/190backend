package tn.project.stopcorona.business.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import tn.project.stopcorona.business.dto.QuestionDTO;
import tn.project.stopcorona.persistence.model.Question;

@Mapper(uses = ResponseMapper.class)
public interface QuestionMapper {
    QuestionMapper INSTANCE = Mappers.getMapper(QuestionMapper.class);

    @Mapping(source = "response", target = "responseDTOS")
    QuestionDTO entityToDto(Question question);

    Question dtoToEntity(QuestionDTO questionDTO);


}
