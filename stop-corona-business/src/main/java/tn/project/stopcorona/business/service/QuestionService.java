package tn.project.stopcorona.business.service;

import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.project.stopcorona.business.dto.QuestionDTO;
import tn.project.stopcorona.business.mapper.QuestionMapper;
import tn.project.stopcorona.persistence.model.Question;
import tn.project.stopcorona.persistence.repository.QuestionRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class QuestionService {

    private final QuestionRepository questionRepository;
    private final QuestionMapper questionMapper = Mappers.getMapper(QuestionMapper.class);


    QuestionService(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Transactional(readOnly = true)
    public List<QuestionDTO> findAllQuestions() {
        List<Question> questions = questionRepository.findAllByOrderByQuestionOrder();
        return questions.stream()
                .map(question -> questionMapper.entityToDto(question))
                .collect(Collectors.toList());
    }
}
