package tn.project.stopcorona.business.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PatientDTOWithConnectedNumber implements Serializable {

    private static final long serialVersionUID = 9174940216865361861L;

    private int connectedPatientsNumber;
    private List<PatientDTO> patients;

}
