package tn.project.stopcorona.business.mapper;

import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import tn.project.stopcorona.business.dto.FormDTO;
import tn.project.stopcorona.persistence.model.Form;

import java.util.ArrayList;
import java.util.List;

@Mapper
public abstract class FormMapper {

    @Mappings({
            @Mapping(target = "questionId", source = "question.id"),
            @Mapping(target = "questionText", source = "question.text")
    })
    public abstract  FormDTO entityToDto(Form form);

    @Mappings({
            @Mapping(target = "response.id", expression = "java(formDTO.getResponseIds().get(0))"),
            @Mapping(source = "questionId", target = "question.id")
    })
    public abstract Form dtoToEntity(FormDTO formDTO);

    @AfterMapping
    protected void setFormDetail(Form to, @MappingTarget FormDTO formDto) {
        formDto.setResponseIds(new ArrayList<>());
        formDto.setResponseTexts(new ArrayList<>());

        formDto.getResponseIds().add(to.getResponse().getId());
        formDto.getResponseTexts().add(to.getResponse().getText());
    }

}
