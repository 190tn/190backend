package tn.project.stopcorona.business.service;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.project.stopcorona.business.dto.FormDTO;
import tn.project.stopcorona.business.dto.PatientDTO;
import tn.project.stopcorona.business.dto.PatientDTOWithConnectedNumber;
import tn.project.stopcorona.business.mapper.FormMapper;
import tn.project.stopcorona.business.mapper.PatientMapper;
import tn.project.stopcorona.persistence.model.Doctor;
import tn.project.stopcorona.persistence.model.Patient;
import tn.project.stopcorona.persistence.model.RequestStatus;
import tn.project.stopcorona.persistence.repository.DoctorRepository;
import tn.project.stopcorona.persistence.repository.PatientRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class PatientService {

    private final PatientRepository patientRepository;
    private final PatientMapper patientMapper = PatientMapper.INSTANCE;
    private final DoctorRepository doctorRepository;
    private final NotificationService notificationService;
    private boolean isNotificationEnabled;

    PatientService(PatientRepository patientRepository,
                   DoctorRepository doctorRepository,
                   NotificationService notificationService,
                   @Value("${stop-corona.notification.enabled:true}") boolean isNotificationEnabled) {
        this.patientRepository = patientRepository;
        this.doctorRepository = doctorRepository;
        this.notificationService = notificationService;
        this.isNotificationEnabled = isNotificationEnabled;
    }

    @Transactional(readOnly = true)
    public PatientDTOWithConnectedNumber getInProgressPatients() {
        List<Patient> patients = patientRepository.findByRequestStatus();
        if (patients == null || patients.isEmpty()) {
            return null;
        }
        int connectedPatientsNumber = 0;
        List<PatientDTO> patientDTOS = new ArrayList<>();
        for (Patient patient : patients) {
            if (patient != null && patient.getConnected() != null && patient.getConnected() && (patient.getCallInProgress() == null || patient.getCallInProgress() == false)) {
                connectedPatientsNumber++;
            }
            patientDTOS.add(patientMapper.entityToDto(patient));
        }
        return PatientDTOWithConnectedNumber.builder()
                .connectedPatientsNumber(connectedPatientsNumber)
                .patients(patientDTOS)
                .build();
    }

    @Transactional
    public PatientDTO addPatient(PatientDTO patientDTO) {
        FormMapper INSTANCE = Mappers.getMapper(FormMapper.class);
        Patient patient = patientRepository.findUuidByNameAndPhoneNumber(patientDTO.getFullName(), patientDTO.getPhoneNumber());
        String uuid = UUID.randomUUID().toString();
        patientDTO.setUuid(uuid);
        if (patient == null) {
            patient = PatientMapper.INSTANCE.dtoToEntity(patientDTO);
            for (FormDTO formDTO : patientDTO.getForms()) {
                patient.addForm(INSTANCE.dtoToEntity(formDTO));
            }
            if(patient.getDelegation() != null && patient.getDelegation().getId() == null){
                patient.setDelegation(null);
            }
            patientRepository.save(patient);
        }
        // TEMP solution for disabled send notification in dev
        // we need to configure environment files
        if (isNotificationEnabled) {
            sendNotificationAndEmailToDoctors();
        }
        return patientDTO;
    }

    public PatientDTO getFirstConnectedPatientForCall(String doctorUserName) {
        Doctor doctor = doctorRepository.findByUserName(doctorUserName).orElse(null);
        if (doctor == null) {
            return null;
        }
        List<Patient> patients = patientRepository.findFirstConnectedPatient(PageRequest.of(0, 1), doctor.getId());
        if (patients == null || patients.isEmpty()) {
            return null;
        }
        Patient firstPatient = patients.get(0);
        int updateStatus = patientRepository.updatePatient(firstPatient.getId(), doctor.getId());
        PatientDTO patientDTO = patientMapper.entityToDto(firstPatient);

        return updateStatus == 1 ? patientDTO : null;
    }

    private void sendNotificationAndEmailToDoctors() {
        notificationService.sendNotificationToSubscribedDoctors();
        notificationService.sendEmailToAllDoctors();
    }

    public void setRequestStatus(Integer patientId, String doctorUsername, RequestStatus requestStatus, String comment) {
        patientRepository.setRequestStatus(patientId, doctorUsername, requestStatus, comment);
    }

    public PatientDTO getDirectCall(String doctorUsername) {
        List<Patient> p = patientRepository.findByRequestStatusAndDoctorUserName(RequestStatus.DIRECT_CALL, doctorUsername);

        if (!p.isEmpty()) {
            return PatientMapper.INSTANCE.entityToDto(p.get(0));
        }
        return null;
    }
}
