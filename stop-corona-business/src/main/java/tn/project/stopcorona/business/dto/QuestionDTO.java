package tn.project.stopcorona.business.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class QuestionDTO implements Serializable {

    private static final long serialVersionUID = 8168593282167790457L;

    private int id;
    private String text;
    private int questionOrder;
    private int score;
    private List<ResponseDTO> responseDTOS = new ArrayList<>();
    private String advice;
}
