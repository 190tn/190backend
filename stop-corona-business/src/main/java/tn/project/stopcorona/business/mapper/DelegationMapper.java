package tn.project.stopcorona.business.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import tn.project.stopcorona.business.dto.DelegationDTO;
import tn.project.stopcorona.business.dto.QuestionDTO;
import tn.project.stopcorona.business.dto.RegionDTO;
import tn.project.stopcorona.persistence.model.Delegation;
import tn.project.stopcorona.persistence.model.Question;
import tn.project.stopcorona.persistence.model.Region;

@Mapper
public interface DelegationMapper {

    DelegationMapper INSTANCE = Mappers.getMapper(DelegationMapper.class);

    DelegationDTO entityToDto(Delegation delegation);

}
