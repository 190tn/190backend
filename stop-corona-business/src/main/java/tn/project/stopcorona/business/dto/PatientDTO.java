package tn.project.stopcorona.business.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tn.project.stopcorona.persistence.model.RequestStatus;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PatientDTO implements Serializable {

    private static final long serialVersionUID = -241746997665873717L;

    private Integer id;
    private String fullName;
    private String phoneNumber;
    private Integer regionId;
    private String regionName;
    private Integer delegationId;
    private String delegationName;
    private Integer requestScore;
    private LocalDateTime requestDate;
    private RequestStatus requestStatus;
    private List<FormDTO> forms;
    private int doctorId;
    private String sessionId;
    private String uuid;
    private Boolean connected;
    private Boolean callInProgress;
}
