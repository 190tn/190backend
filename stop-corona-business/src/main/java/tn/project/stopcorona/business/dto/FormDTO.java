package tn.project.stopcorona.business.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FormDTO implements Serializable {

    private static final long serialVersionUID = -1942164043715816017L;

    private int questionId;
    private String questionText;
    private List<Integer> responseIds;
    private List<String> responseTexts;
    private int patientId;
    private int score;
}
