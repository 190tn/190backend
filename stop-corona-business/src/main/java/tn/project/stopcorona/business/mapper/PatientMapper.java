package tn.project.stopcorona.business.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import tn.project.stopcorona.business.dto.PatientDTO;
import tn.project.stopcorona.persistence.model.Patient;

@Mapper(uses = {FormMapper.class})
public interface PatientMapper {

    PatientMapper INSTANCE = Mappers.getMapper(PatientMapper.class);

    @Mapping(source = "region.id", target = "regionId")
    @Mapping(source = "region.name", target = "regionName")
    @Mapping(source = "delegation.id", target = "delegationId")
    @Mapping(source = "delegation.name", target = "delegationName")
    PatientDTO entityToDto(Patient patient);

    @Mappings({
            @Mapping(source = "regionId", target = "region.id"),
            @Mapping(source = "delegationId", target = "delegation.id")
    })
    Patient dtoToEntity(PatientDTO patientDTO);
}
