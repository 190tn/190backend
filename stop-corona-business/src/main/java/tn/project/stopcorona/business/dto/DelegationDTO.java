package tn.project.stopcorona.business.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DelegationDTO implements Serializable {

    private static final long serialVersionUID = -1944990015429915083L;

    private Integer id;
    private String name;
}
