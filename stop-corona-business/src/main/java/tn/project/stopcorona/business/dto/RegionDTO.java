package tn.project.stopcorona.business.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tn.project.stopcorona.persistence.model.Delegation;

import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegionDTO implements Serializable {

    private static final long serialVersionUID = -1944990015429915083L;

    private Integer id;
    private String name;
    private Set<DelegationDTO> delegations;
}
